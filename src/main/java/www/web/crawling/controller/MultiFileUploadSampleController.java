package www.web.crawling.controller;

import java.util.HashMap;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import www.web.crawling.common.service.MultiFileUploadService;
import www.web.crawling.dto.MultiFileUploadDto;



@Controller
public class MultiFileUploadSampleController implements HandlerExceptionResolver {
	
	private Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	private MappingJacksonJsonView jsonViewer;
	
	
	@Inject
	private MultiFileUploadService multiFileUploadService;
	
	
	@RequestMapping(value = "/mulitFileUploadSample", method = {RequestMethod.GET})
	public String multiFileUpload(Locale locale, Model model , HttpServletRequest request,HttpServletResponse response) {
		log.info("multiFileUploadSample Start");
		return "multiFileUpload/multiFileUploadSample";
	}
	
	
	//파일업로드
	@RequestMapping(value = "/multiFileUploadJson.json", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody ModelAndView multiFileUploadJson(@ModelAttribute  MultiFileUploadDto paramDTO  /*,final Part productImage*/
    		, Locale locale
            , Model map
            , HttpServletRequest request
            , HttpServletResponse response
            , HttpSession session)throws Exception{
		
		HashMap<String,Object> jsonMap = new HashMap<String,Object>();
		ModelAndView model = new ModelAndView(this.jsonViewer, "result", jsonMap);
		
		
		//파일업로드 처리
		this.multiFileUploadService.setFileToServerCopy(request);
		
		
		jsonMap.put("test", "testJson");
		
		
		
		
		//log.info(productImage.getSubmittedFileName() + " : " + productImage.getSize());
		/*
		log.info("fileName===>" + productImage.getName() + " : " + productImage.getSize() + " : " + productImage.getSubmittedFileName());
		*/
		
		return model;
	}
	
	
	
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object obj, Exception ex) {
		// TODO Auto-generated method stub
		HashMap<String,Object> failMap = new HashMap<String,Object>();
		ModelAndView model = new ModelAndView(this.jsonViewer, "result", failMap);;
		if(ex instanceof MaxUploadSizeExceededException){
			failMap.put("fileUploadFail", "파일용량이 100M 이상인 파일은 업로드가 불가능합니다.");
		}
		return model;
	}
	
	
	
	
	
	
}
