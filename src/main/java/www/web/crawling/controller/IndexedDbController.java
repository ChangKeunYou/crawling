package www.web.crawling.controller;

import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import www.web.crawling.common.service.MultiFileUploadService;

@Controller
public class IndexedDbController {
	
	
	
	private Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	private MappingJacksonJsonView jsonViewer;
	
	
	@Inject
	private MultiFileUploadService multiFileUploadService;  
	
	
	@RequestMapping(value = "/indexed", method = {RequestMethod.GET})
	public String multiFileUpload(Locale locale, Model model , HttpServletRequest request,HttpServletResponse response) {
		return "indexeddb/indexeddb";
	}
	
	
	
}
