package www.web.crawling.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MaginController {

	private static final Logger logger = LoggerFactory.getLogger(MaginController.class);
    
	@RequestMapping(value = "/magic/magicMain", method = RequestMethod.GET)
	public String magicMain(Locale locale, Model model , HttpServletRequest request,HttpServletResponse response) {
		logger.info("magicSample");
		return "magic/magic";
	}
}
