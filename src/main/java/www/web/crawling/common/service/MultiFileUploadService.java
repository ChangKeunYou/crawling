package www.web.crawling.common.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;


public interface MultiFileUploadService {

	public  void  setFileToServerCopy(HttpServletRequest request) throws Exception;
	
}
