package www.web.crawling.common.service.impl;

import java.io.File;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import www.web.crawling.common.service.MultiFileUploadService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

@Service(value="multiFileUploadService")
public class MultiFileUploadServiceImpl implements MultiFileUploadService {

	private Log log = LogFactory.getLog(this.getClass());
	
	@Override
	public void setFileToServerCopy(HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		Collection<Part> parts = request.getParts();
		String path = "/Users/youchangkeun/Desktop/FileUploadTest/";
		  
		
	    File fileSaveDir = new File(path);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }
		
		for(Part part : parts){
			if(part.getSubmittedFileName() == null){
				continue;
			}
			part.write(path + part.getSubmittedFileName());
			//log.info("fileName=>" + part.getSubmittedFileName() + " : " + part.getSize() + " : " + part.getName());
		}
		
		
	}
	
	
	
	
}
