package www.web.crawling.dto;

import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.Part;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.apache.ibatis.type.Alias;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;


@Alias("file.multiFileUploadDto")
public class MultiFileUploadDto {
	
	private String cpu;
	
	private String mainboard;
	
	private String hdd;
	
	private String ram;
	
	//private MultipartFile productImage;
	
	//servlet spec3.0 file upload element
	//private Part[] productImage; //씨발 part는 객체는 dto 안에 있음 웹에서 받질 못 함..
	
	//private MultipartFile[] productImage;
	
	
	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getMainboard() {
		return mainboard;
	}

	public void setMainboard(String mainboard) {
		this.mainboard = mainboard;
	}

	public String getHdd() {
		return hdd;
	}

	public void setHdd(String hdd) {
		this.hdd = hdd;
	}

	public String getRam() {
		return ram;
	}

	public void setRam(String ram) {
		this.ram = ram;
	}

	
	/*
	public MultipartFile[] getProductImage() {
		return productImage;
	}

	public void setProductImage(MultipartFile[] productImage) {
		this.productImage = productImage;
	}
	*/
	
	/*
	public Part[] getProductImage() {
		return productImage;
	}
	
	public void setProductImage(Part[] productImage) {
		this.productImage = productImage;
	}
	*/
	
	/*
	public MultipartFile getProductImage() {
		return productImage;
	}

	public void setProductImage(MultipartFile productImage) {
		this.productImage = productImage;
	}
	*/

	/*
	public Part getProductImage() {
		return productImage;
	}

	public void setProductImage(Part productImage) {
		this.productImage = productImage;
	}
	*/
	
	
	
}
