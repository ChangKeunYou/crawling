<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String root = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>magicSample</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<%=root%>/resources/js/magic/magicsuggest.css"/>

<script type="text/javascript"  src="<%=root%>/resources/js/jquery-1.11.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript"  src="<%=root%>/resources/js/magic/magicsuggest.js"></script>
<script type="text/javascript">
var dataSample1 = [
                   					'한주희',
                   					'이태성',
                   					'이순신',
                   					'땡칠이',
                   					'유창근',
                   					'이진원'
                				];

var dataSample2 = [
							{
							    id: 1,
							    name: 'Location',
							    nb: 34
							}, 
							{
							    id: 2,
							    name: 'Keyword',
							    nb: 106
							}
				];

	$(function(e){
		$("#addArea").click(function(e){
			var size = $("#tableArea > tbody").children("tr").size() + 1
			var html = "";
			html += "<tr id='row_" +size+ "'>";
				html += "<td align='center'>" + size + "</td>";
				html += "<td>테스트" + size + "</td>";
				html += "<td align='center'><div id='ms-filter-" + size + "' style='width: 200px;'></div></td>"
			html += "</tr>"
			
			$(html).appendTo("#tableArea > tbody");
			
			//이부분에서 다시 이벤트를 태워서 추가된 row에 자동검색을 만드어준다.
		    var ms2 =  $('#ms-filter-' + size).magicSuggest({
		        placeholder: '봐붜땡칠이',
		        data: dataSample1
		    });
		
	    	$(ms2).on('selectionchange', function(e,m){
	      	  alert("values: " + JSON.stringify(this.getValue()));
	      	});				
			
		});
		
		
		//default
		
		
		
	   var ms =  $('#ms-filter-1').magicSuggest({
	        placeholder: '-선택-',
	        allowFreeEntries: false,
	        width:"10%",
	        data: dataSample2,
	        selectionPosition: 'bottom',
	        selectionStacked: true,
	        selectionRenderer: function(data){
	            return data.name + ' (<b>' + data.nb + '</b>)';
	        }
	    });
			
    	$(ms).on('selectionchange', function(e,m){
    	  alert("values: " + JSON.stringify(this.getValue()));
    	});
	
		
	});
</script>

</head>

<body>
	
	<div>
		<table id="tableArea" border="1" style="border: 1px red solid; width: 50%">
				<caption>magic Suggest Sample</caption>
					<colgroup>
						<col style="width:5%">
						<col style="width:30%">
						<col style="width:65%">
					</colgroup>
				<thead>
					<tr>
						<td align="center">순번</td>
						<td align="center">업무명</td>
						<td align="center">자동검색영역</td>
					</tr>
				</thead>
				<tbody>
					<tr id="row_1">
						<td align="center">1</td>
						<td>테스트1</td>
						<td align="center">
							<div id="ms-filter-1" style="width: 200px;"></div>
						</td>						
					</tr>
				</tbody>
		</table>
	</div>
	
	<br /> <br />
	
	<div id="buttonArea">
		<input type="button" id="addArea" value="행추가" />
	</div>
		
	
</body>
</html>