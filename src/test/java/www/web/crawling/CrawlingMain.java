package www.web.crawling;


import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;


public class CrawlingMain extends CrawlingAbstractJunit {

	private long start = 0;
	private long end = 0;
	private ArrayList<String> crawlingUrl = new ArrayList<String>();
	
	
	@Override
	public void start() {
		// TODO Auto-generated method stub
		log.info("=============================Crawling Start=============================");
		this.start = System.currentTimeMillis();
		//crawlingUrl.add("http://prod.danawa.com/list/?defSite=PC&cate_c1=861&cate_c2=873&cate_c3=959&page=1");
		for(int i = 1; i <= 3; i++){
			crawlingUrl.add("http://prod.danawa.com/list/item.php?nPreDefineSiteCode=1&cate_c1=861&cate_c2=873&cate_c3=959&cate_c4=0&cate_depth=3&mode=price&sDefSite=PC&srch_price1=0&page=" + i);	
		}
		
		/*
		for(int i = 1; i <= 100; i++){
			crawlingUrl.add("http://prod.danawa.com/list/?defSite=PC&cate_c1=861&cate_c2=873&cate_c3=959&page=" + i);//최초 해당 url 에서 페이징 수를 얻어온다.
		}
		*/
		//crawlingUrl.add("http://prod.danawa.com/list/?defSite=PC&cate_c1=861&cate_c2=873&cate_c3=959");//최초 해당 url 에서 페이징 수를 얻어온다.
		//crawlingUrl.add("http://prod.danawa.com/info/?pcode=2739042&cate1=861&cate2=873&cate3=960&cate4=0");//amd cpu
		//crawlingUrl.add("http://prod.danawa.com/info/?pcode=2593905&cate1=861&cate2=875&cate3=968&cate4=0");//mainBoard
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		this.end = System.currentTimeMillis();
		log.info("Crawling Time=>" + (this.end - this.start)/1000.0 + " seconds");
		log.info("==============================Crawling End============================");
	}

	@Override
	public void run_Test(){
		// TODO Auto-generated method stub
		//Thread.sleep(2000);
		HttpClient httpClient = null;
		HttpGet httpGet = null;
		
		try{
			httpClient = new DefaultHttpClient();
			for(String url : crawlingUrl){
				log.info("Crawling Url=>" + url);
				
				
				httpGet = new HttpGet(url);
				//httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				httpGet.addHeader("Referer","http://prod.danawa.com/list/search_option.inc.php?defSite=PC&cate1=861&cate2=873&skin=half&mode=price&nPreDefineSiteCode=1");//referer 값을 가지고 가야 포워딩이 안됨.
				//httpGet.addHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10) AppleWebKit/600.1.25 (KHTML, like Gecko) Version/8.0 Safari/600.1.25");
				
				
				
				//httpGet.set
				httpClient.execute(httpGet, new CrawlingBasicResponseHandlerExtends());
			}
			
			
			
		}catch(Exception e){
			log.error("CrawlingException=>" + e.getMessage());
		}finally{}
		
		
		
	}
	
	
	
	
	
}
