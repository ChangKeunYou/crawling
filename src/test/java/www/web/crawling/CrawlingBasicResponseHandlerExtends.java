package www.web.crawling;

import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CrawlingBasicResponseHandlerExtends extends BasicResponseHandler{
	
	private Log log = LogFactory.getLog(this.getClass());
	
	
	@Override
	public String handleResponse(HttpResponse response)throws HttpResponseException, IOException {
		// TODO Auto-generated method stub
		String url = "http://prod.danawa.com/list/";
		String res = new String(super.handleResponse(response).getBytes("8859_1"),"euc-kr");
		
		Document doc = Jsoup.parse(res);
		
		log.info("===parsing===>" + doc);
		
		
		/*
		Element searchIframe = doc.select("#IFRAME_SrchOption").first();
		String searchUrl = searchIframe.attr("src");
		
		log.info("===>" +  url + searchUrl);
		
		
		Document siframe =  Jsoup.connect(url + searchUrl).get();
		
	log.info("===>" + siframe.toString());
		*/
		
		//Elements eles = doc.select("#dnw_content");
		
		//log.info("=====>" + eles.size());
		
		
		//for(Element ele : eles){
		//	log.info("======>" + ele.toString());
		//}
		
		
		
		//Element searchIframe = doc.select("#IFRAME_SrchOption").first();
		
		//Element datListIframe = doc.select("#IFRAME_ProdList").first();
		
		
		//Element ulEle = doc.select("#numberlist").first();
		
		//log.info("======htmlParser====>" + doc.toString());
		
		//Elements rows = doc.select("#tableInfo > tbody > tr");
		
		//for(Element row : rows){//tr 동적
		//	log.info("====>" + row.toString());
		//}
		
		//log.info("html====>" + doc.toString());
		
		return res;
	}
	
}
