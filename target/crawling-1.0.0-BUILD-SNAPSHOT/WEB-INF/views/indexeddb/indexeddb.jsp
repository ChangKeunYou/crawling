<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String root = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>IndexedDB example</title>

<style type="text/css">
		 .debugClass{
 			border: 1px solid #000000; 
 			color:#00ff00; 
 			background:#000000;  
  	 	  } 
</style>
<script type="text/javascript"  src="<%=root%>/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"  src="<%=root%>/resources/js/jquery.form.js"></script>

<script type="text/javascript">
//prefixes of implementation that we want to test
window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
 
//prefixes of window.IDB objects
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange
 
if (!window.indexedDB) {
    window.alert("Your browser doesn't support a stable version of IndexedDB.")
}

const customerData = [
                      { id: "00-01", name: "Bill", age: 35, email: "bill@company.com" },
                      { id: "00-02", name: "Donna", age: 32, email: "donna@home.org"  , c : [{a:"111",b:"222"}]}
                   ];

var db;
var request = window.indexedDB.open("yckIndexDb", 1);



request.onerror = function(event) {
	  d.log("error: ");
};
	 
request.onsuccess = function(event) {
	  db = request.result;
	  d.log("success: "+ db);
};
	 




$(function(){


	request.onupgradeneeded = function(event) {
		   var db = event.target.result;
	       var objectStore = db.createObjectStore("customers", {keyPath: "id"});
	       for (var i in customerData) {
	               objectStore.add(customerData[i]);       
	       }
	 	
	}
	
	$("#read").click(function(e){
		var transaction = db.transaction(["customers"]);
        var objectStore = transaction.objectStore("customers");
        var request = objectStore.get("00-03");
        request.onerror = function(event) {
          alert("Unable to retrieve daa from database!");
        };
        request.onsuccess = function(event) {
          // Do something with the request.result!
          if(request.result) {
                alert("Name: " + request.result.name + ", Age: " + request.result.age + ", Email: " + request.result.email);
          } else {
                alert("Kenny couldn't be found in your database!");  
          }
        };
	});

	$("#readAll").click(function(e){
		  var objectStore = db.transaction("customers").objectStore("customers");
		  
	        objectStore.openCursor().onsuccess = function(event) {
	          var cursor = event.target.result;
	          if (cursor) {
	                alert("Name for id " + cursor.key + " is " + cursor.value.name + ", Age: " + cursor.value.age + ", Email: " + cursor.value.email);
	                cursor.continue(); 
	          }
	          else {
	                alert("No more entries!");
	          }
	        };     
	});

	$("#add").click(function(e){
		var request = db.transaction(["customers"], "readwrite")
        .objectStore("customers")
        .add({ id: "00-03", name: "Kenny", age: 19, email: "kenny@planet.org" });
                       
		request.onsuccess = function(event) {
		        alert("Kenny has been added to your database.");
		};
		 
		request.onerror = function(event) {
		        alert("Unable to add data\r\nKenny is aready exist in your database! ");        
		}
	});

	$("#remove").click(function(e){
        var request = db.transaction(["customers"], "readwrite")
                .objectStore("customers")
                .delete("00-03");
        request.onsuccess = function(event) {
          alert("Kenny's entry has been removed from your database.");
        };
	});
	
	
});


var d = {
	    t: {}, //json
	    a: [], //array
	    tempFun: null, //tempFun
	    log: function(msg) {

	        var message = msg.toString();

	        if ($("#debugColsol").size() <= 0) {
	            $("<div id='debugColsol'></div>").prependTo("body");
	            //$("<div id='debugColsol'></div>").appendTo("body");
	        }
	        var $selted = jQuery("#debugColsol").show();
	        $selted.removeClass("debugClass");
	        if (message != null && message != "") {
	            jQuery("<div></div>").addClass("debugClass").fadeIn("slow").text(msg).appendTo($selted);
	        } else {
	            jQuery("<div></div>").addClass("debugClass").fadeIn("slow").text("디버깅 메세지가 없습니다 확인해주세요").appendTo($selted);
	        }
	    } //,확장하면서 사용..
	};

</script>

</head>
<body leftmargin="0" topmargin="0">
	
	<br /><br /><br />
	<input type="button" id="read" name="read" value="read" />
	<input type="button" id="readAll" name="readAll" value="readAll" />
	<input type="button" id="add" name="add" value="add" />
	<input type="button" id="remove" name="remove" value="remove" />		
	
</body>
</html>