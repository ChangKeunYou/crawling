<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String root = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>multiFileUplaodSample</title>

<style type="text/css">
		 .debugClass{
 			border: 1px solid #000000; 
 			color:#00ff00; 
 			background:#000000;  
  	 	  }
  	 	  
  	 	   	 	  fieldset { 
    display: block;
    margin-left: 2px;
    margin-right: 2px;
    padding-top: 0.35em;
    padding-bottom: 0.625em;
    padding-left: 0.75em;
    padding-right: 0.75em;
    border: 2px groove (internal value);
}
  	 	  
</style>
<script type="text/javascript"  src="<%=root%>/resources/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"  src="<%=root%>/resources/js/jquery.form.js"></script>

<script type="text/javascript">
	$(document).ready(function(e){
		//d.log("Ajax multi File Upload..");
		//d.log($("#btnArea").size());
		
		//파일추가
		$("#plus").click(function(e){
			if(f_util.fileLeanth($("#fileArea")) >= 5){
				alert("파일업로드는 5개 까지 가능합니다.");
				return;		
			}else{
				f_util.fileAdd($("#fileArea"));
			}
		});
		
		//파일삭제 동적으로 추가되기 때문에 delegate나 on으로 사용
		$("#fileArea").on("click","input[name='minus']",function(e){ 
			f_util.fileDel($(this));
		});
		
		//파일 선택시 미리보기 기능 추가(파일 엘리먼트는 동적으로 추가)
		$("#fileArea").on("change","input[type='file']",function(e){
			f_util.readImage(this);
		});
		
		
		
		//저장
		$("#save").click(function(e){
			var o = {};
			o.url = "<%=root%>/multiFileUploadJson.json";
			f_util.fileReg(o);
			
		});

		$("#init").click(function(e){
			alert("개발중..");
		});
		
	});

	
	var f_util = {
			
			fileAdd : function(o){//파일 엘리먼트 추가
				var fileSize = o.find("input[type='file']").size();
				var fileHtml = $("<div></div>").append(o.find("input[type='file']:first").clone()).append("<input type='button' name='minus' value='-' />").append("<img width='60' height='70' style='display: none;' />");		
				$(fileHtml).appendTo(o);
				$(o).find("input[type='file']:last").attr({"id":"productImage_" + (fileSize + 1)});
				$(o).find("img:last").attr({"id":"imgpreview_" + (fileSize + 1)});
			},
				
			fileDel : function(o){//파일 엘리먼트 삭제
				o.parent().remove();
			},

			fileLeanth : function(o){//파일의 총 엘리먼트 수를 반환
				var fileSize = o.find("input[type='file']").size();
				return fileSize;
			},
			
			filedup : function(o){//같은 파일이 있는지 검사 차후 진행 하겠음..
				return true;
			},
			
			fileReg : function(o){
				//fnShowLoading();
				
				$("#myform").ajaxSubmit({
					url: o.url,
					type:'POST',
					success:function(data, statusText, xhr){

						console.log(data);
						
						//f_util.f_clear();
						//fnCloseLoading();
					}
				}); 
			},
			readImage : function(o){
				  if (o.files && o.files[0]) {
	                    var reader = new FileReader(); //파일을 읽기 위한 FileReader객체 생성
	                    reader.onload = function (e) {
	                    	//파일 읽어들이기를 성공했을때 호출되는 이벤트 핸들러
	                        //$('#blah').attr('src', e.target.result);
	                    	//alert("====>" + $(o).next().next());
	                    	//alert(e.target.result);
	                    	$(o).next().next().css("display","block").attr("src",e.target.result);
	                        //이미지 Tag의 SRC속성에 읽어들인 File내용을 지정
	                        //(아래 코드에서 읽어들인 dataURL형식)
	                    }                   
	                    reader.readAsDataURL(o.files[0]);
	                    //File내용을 읽어 dataURL형식의 문자열로 저장
	                }
			}
			
	};
	

	var d = {
		    t: {}, //json
		    a: [], //array
		    tempFun: null, //tempFun
		    log: function(msg) {

		        var message = msg.toString();

		        if ($("#debugColsol").size() <= 0) {
		            $("<div id='debugColsol'></div>").prependTo("body");
		            //$("<div id='debugColsol'></div>").appendTo("body");
		        }
		        var $selted = jQuery("#debugColsol").show();
		        $selted.removeClass("debugClass");
		        if (message != null && message != "") {
		            jQuery("<div></div>").addClass("debugClass").fadeIn("slow").text(msg).appendTo($selted);
		        } else {
		            jQuery("<div></div>").addClass("debugClass").fadeIn("slow").text("디버깅 메세지가 없습니다 확인해주세요").appendTo($selted);
		        }
		    } //,확장하면서 사용..
		};
	
</script>

</head>
<body topmargin="0" leftmargin="0">

	<form id="myform" name="myform" method="post" enctype="multipart/form-data">
		<input type="hidden" id="param1" name="value1" />
		<input type="hidden" id="param2" name="value2" />
		<input type="hidden" id="param3" name="value3" />
		
		
		
		 <fieldset id="btnsArea">
		    <legend>버튼영역</legend>
		     <input type="button" id="save" name="save" value="저장" />
		     <input type="button" id="init" name="init" value="초기화" />
		  </fieldset>
		  
		 <br/>
		 
		  <fieldset id="contentsArea">
		     <legend>컨텐츠영역</legend>
		     
		     <div style="width: 70%;">
		     	 <table id="contentsTable" cellpadding="0" cellspacing="0" border="1">
		     	 	<caption style="display: none;">컨텐츠</caption>
		     	 	<colgroup>
		     				<col style="width:15%;">
							<col style="width:35%;">
							<col style="width:15%;">
							<col style="width:35%;">
		     	 	</colgroup>
		     	 	<tbody>
		     	 		<tr>
		     	 			<td>CPU</td>
		     	 			<td>
		     	 				<input type="text" id="cpu" name="cpu" value="제온 CPU" /> 
		     	 			 </td>
		     	 			<td>메인보드</td>
		     	 			<td>
		     	 				<select id="mainboard" name="mainboard">
		     	 					<option value="asusMainboard">아수스 메인보드</option>
		     	 					<option value="pk5Mainboard">PK5 메인보드</option>
		     	 					<option value="intelMainboard">인텔 메인보드</option>
		     	 				</select>
		     	 			</td>
		     	 		</tr>
		     	 		
		     	 		<tr>
		     	 			<td>HDD</td>
		     	 			<td>
		     	 				<input type="text" id="hdd" name="hdd" value="Seagate" /> 
		     	 			 </td>
		     	 			<td>RAM</td>
		     	 			<td>
		     	 				<select id="ram" name="ram">
		     	 					<option value="lgram">엘지렘5기가</option>
		     	 					<option value="samsungram">삼성렘10기가</option>
		     	 					<option value="intelram">인텔렘</option>
		     	 				</select>
		     	 			</td>
		     	 		</tr>
		     	 		
		     	 		<tr>
		     	 			<td>
		     	 					이미지 </br>
		     	 					<input type="button" id="plus" name="plus" value="+" />
		     	 			</td>
		     	 			<td colspan="3">
		     	 				<div id="fileArea">
		     	 					<div>
		     	 						<input type="file" name="productImage" id="productImage_1"  /><input type='button' name='minus' value='-' style="display: none;" /><img id="imgpreview_1" width="60px" height="60px"  style="display: none;" />
		     	 					</div>
		     	 				</div>
		     	 			</td>
		     	 		</tr>
		     	 		
		     	 	</tbody>
		     	 </table>
		     </div>
		     <img/>
		  </fieldset>
				
			
	</form>

		
</body>
</html>